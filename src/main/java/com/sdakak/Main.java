package com.sdakak;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/*
  Simple class that demonstrates scafolding code to read/write json and make post/get requests
 */
public class Main {

  static Gson gson = new GsonBuilder().setDateFormat("MM/dd/yyyy").create();
  static SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

  public static void main(String args[]) throws Exception {
    readAndWriteJson();
    makePostAndGetRequests();
  }

  private static void readAndWriteJson() {
    List<President> presidentList = null;

    // Read Json
    try (Reader reader = new FileReader("src/main/resources/presidents.txt")) {
      // To read single class President president = gson.fromJson(reader, President.class);
      presidentList = gson.fromJson(reader, new TypeToken<List<President>>() {
      }.getType());
      presidentList.forEach(president -> {
        // Change Date Object
        Calendar cal = Calendar.getInstance();
        cal.setTime(president.lastModified);
        cal.add(Calendar.YEAR, 1);
        president.setLastModified(cal.getTime());
        System.out.println(president);
      });
    } catch (IOException e) {
      e.printStackTrace();
    }

    // Write Changes back to Json
    try (FileWriter writer = new FileWriter("src/main/resources/presidents.txt")) {
      gson.toJson(presidentList, new TypeToken<List<President>>() {
      }.getType(), writer);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void makePostAndGetRequests() throws Exception {
    System.out.println("Read and Wrote Above to file");
    System.out.println();

    // Make POST Request
    // With manual fields
    String server = "https://reqres.in/api/users";
    HttpResponse<String> jsonResponse = Unirest
        .post(server)
        .header("accept", "application/json")
        .field("name", "obama")
        .field("job", "president")
        .asString();
    System.out.println("Made Post Request 1:");
    System.out.println("Status: " + jsonResponse.getStatus());
    System.out.println("Body: " + jsonResponse.getBody());
    System.out.println();

    // With json object as body
    President president = new President("George Washington",
        formatter.parse("3/4/1797"), formatter.parse("1/1/2018"), 300, 10.50, false);
    System.out.println("Test: ");
    System.out.println(gson.toJson(president, President.class));
    jsonResponse = Unirest
        .post(server)
        .header("accept", "application/json")
        .body(gson.toJson(president, President.class))
        .asString();
    System.out.println("Made Post Request 2:");
    System.out.println("Status: " + jsonResponse.getStatus());
    System.out.println("Body: " + jsonResponse.getBody());
    System.out.println();

    // Make GET Request
    jsonResponse = Unirest
        .get(server)
        .queryString("id", "1")
        .header("accept", "application/json")
        .asString();
    System.out.println("Made GET Request:");
    System.out.println("Status: " + jsonResponse.getStatus());
    System.out.println("Body: " + jsonResponse.getBody());

    Unirest.shutdown();
  }
}
