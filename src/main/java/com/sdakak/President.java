package com.sdakak;

import java.util.Date;

public class President {

  String name;
  Date dob;
  Date lastModified;
  int age;
  double netWorth;
  boolean current;

  public President(String name, Date dob, Date lastModified, int age, double netWorth,
      boolean current) {
    this.name = name;
    this.dob = dob;
    this.lastModified = lastModified;
    this.age = age;
    this.netWorth = netWorth;
    this.current = current;
  }

  public Date getLastModified() {
    return lastModified;
  }

  public void setLastModified(Date lastModified) {
    this.lastModified = lastModified;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getDob() {
    return dob;
  }

  public void setDob(Date dob) {
    this.dob = dob;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public double getNetWorth() {
    return netWorth;
  }

  public void setNetWorth(double netWorth) {
    this.netWorth = netWorth;
  }

  public boolean isCurrent() {
    return current;
  }

  public void setCurrent(boolean current) {
    this.current = current;
  }

  @Override
  public String toString() {
    return "President{" +
        "name='" + name + '\'' +
        ", dob=" + dob +
        ", lastModified=" + lastModified +
        ", age=" + age +
        ", netWorth=" + netWorth +
        ", current=" + current +
        '}';
  }
}
