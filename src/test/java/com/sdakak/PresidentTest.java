package com.sdakak;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import junit.framework.TestCase;
import org.junit.Test;

public class PresidentTest extends TestCase {

  President president = null;

  @Override
  protected void setUp() throws ParseException {
    SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    president = new President("George Washington",
        formatter.parse("3/4/1797"), formatter.parse("1/1/2018"), 300, 10.50, false);
  }

  @Test
  public void testBark() {
    assertEquals("George Washington", president.getName());
  }

}
