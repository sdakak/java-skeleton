This is a bare bones starter project that you can clone and quickly start developing.

It does things like set up basic maven structure and pom file with just the essentials like guava, junit, gson for json parsing and unirest for making rest requests.

It also has sample code to read, parse and write json and make basic REST requests so you aren't fiddling around with those thing forever
